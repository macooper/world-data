interface ChartTrace {
    x: number[];
    y: number[];
    z: number[];
    text?: string[];
    w?: number[];
    marker?: any;
    mode?: 'markers';
    type: 'scatter3d';
}