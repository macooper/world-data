export default class Simulator {
    // initialize the positions of the points arbitrarily
    public static initialize(locations: PingLocation[]) {
        const points = locations.filter(p => p)
            .map<ForcedLocation>(p => ({
                ...p, 
                forces: zero() 
            }));

        for (const k of points) {
            const name = k.name + "ZZZ";
            k.x = name.charCodeAt(0);
            k.y = name.charCodeAt(1);
            k.z = name.charCodeAt(2);
            k.absForce = zero();
        }

        return points;
    }

    public static step(points: ForcedLocation[]) {
        // compute the "forces"
        points.forEach(p => {
            p.absForce = zero();
            p.forces = points.reduce(
                (f, o) => {
                    if (p === o || !p.links[o.id] || !p.links[o.id].distance) return f;
                    const diff = subtract(p, o);
                    const distance = magnitude(diff);
                    if (distance === 0) return f;
                    const offset = p.links[o.id].distance - distance;
    
                    // force decreases with distance
                    const force = multiply(diff, offset / (distance * p.links[o.id].distance));
    
                    p.absForce = add(p.absForce, abs(force));
                    return {
                        ...add(f, force),
                        count: f.count+1
                    }
                },
                zero()
            )
        });
    
        // use the forces to move the points
        //let stepSize = points.length / points.reduce((t, p) => t + (p.forces.count ? magnitude(p.forces) : 0), 0);
        let maxForce = points.reduce((m,p) => Math.max(m,magnitude(p.forces)), 0);
        //console.log(`step: ${stepSize}\t max: ${maxForce}`)
        const stepSize = 200 * Math.min(100, /*stepSize,*/ 1 / maxForce);
        points.forEach(p => {
            if (p.forces.count === 0) return;
            const shift = multiply(p.forces, stepSize);
            p.x += shift.x;
            p.y += shift.y;
            p.z += shift.z;
        });
    
        return stepSize;
    }

}

//#region Vector functions
const zero = () => ({x:0, y:0, z:0, count:0});
function add(a: Vector, b: Vector|number): Vector {
    if (typeof b === "number")
        return {
            x: a.x + b,
            y: a.y + b,
            z: a.z + b
        };
    return {
        x: a.x + b.x,
        y: a.y + b.y,
        z: a.z + b.z
    };
}
function subtract(a: Vector, b: Vector|number): Vector {
    if (typeof b === "number")
        return {
            x: a.x - b,
            y: a.y - b,
            z: a.z - b
        };
    return {
        x: a.x - b.x,
        y: a.y - b.y,
        z: a.z - b.z
    };
}
function magnitude(v: Vector) {
    return Math.sqrt(v.x**2 + v.y**2 + v.z**2);
}
function multiply(v: Vector, b: number): Vector {
    return {
        x: v.x * b,
        y: v.y * b,
        z: v.z * b
    };
}
function abs(a: Vector): Vector {
    return {
        x: Math.abs(a.x),
        y: Math.abs(a.y),
        z: Math.abs(a.z)
    };
}
//#endregion