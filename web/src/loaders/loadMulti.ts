export function Load() {
    const rawData: PingLocation[] = require('../assets/multilateration.json');

    return rawData.map<PingLocation>((f, id) => 
        ({
            id,
            ...f,
            country: '',
            computed: true,
            links: []
        })
    );
}