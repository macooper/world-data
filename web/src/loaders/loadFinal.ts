export function Load() {
    const rawData: PingLocation[] = require('../assets/final.json');

    return rawData.map<PingLocation>((f, id) => 
        ({
            id,
            ...f,
            country: '',
            computed: true,
            links: []
        })
    );
}