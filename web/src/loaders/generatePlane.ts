export function Load() {
    const points = 100;
    const diameter = 200;

    const distanceErrorPercent = 0.1;
    const linkFillPercent = 0.3;

    let locations: PingLocation[] = [];

    for (let id=0; id<points; id++) {
        const pt: PingLocation = {
            id,
            computed: false,
            links: [],
            name: ""+Math.floor(100 + Math.random() * 800),
            country: "",
            x: diameter * Math.random(),
            y: diameter * Math.random(),
            z: 0
        };
        locations.push(pt);
    }
    for (const pt1 of locations) {
        for (let id2=pt1.id+1; id2<locations.length; id2++) {
            if (Math.random() > linkFillPercent) continue;
            const pt2 = locations[id2];
            const distance = Math.sqrt((pt1.x-pt2.x)**2 + (pt1.y-pt2.y)**2) 
                * (2 * distanceErrorPercent * Math.random() + 1 - distanceErrorPercent);
            pt1.links[id2] = {
                source: pt1,
                destination: pt2,
                distance
            };
            pt2.links[pt1.id] = {
                source: pt2,
                destination: pt1,
                distance
            };
        }

        //console.log(`${pt1.x.toFixed(2)}\t${pt1.y.toFixed(2)}\t${pt1.z.toFixed(2)}`)

        pt1.x = pt1.y = pt1.z = 0;
    }
    

    return locations;
}