export function Load() {
    const rawData: FlightInfo[] = require('../timetable.json');

    // convert the raw data into something more usable
    let locations: PingLocation[] = [];

    for (const flight of rawData) {
        if (!locations.some(loc => loc.name === flight.from))
            locations.push({
                id: locations.length,
                name: flight.from,
                country: '',
                computed: false,
                links: [],
                x: 0,
                y: 0,
                z: 0
            });
        if (!locations.some(loc => loc.name === flight.to))
            locations.push({
                id: locations.length,
                name: flight.to,
                country: '',
                computed: false,
                links: [],
                x: 0,
                y: 0,
                z: 0
            });
    }

    for (const flight of rawData) {
        const sourceLocation = locations.find(loc => loc.name === flight.from);
        const destinationLocation = locations.find(loc => loc.name === flight.to);
        if (!sourceLocation || !destinationLocation || !flight.distance) continue;
        if (!sourceLocation.links[destinationLocation.id])
            sourceLocation.links[destinationLocation.id] = {
                source: sourceLocation,
                destination: destinationLocation,
                distance: +flight.distance / 10
            };
        if (!destinationLocation.links[sourceLocation.id])
            destinationLocation.links[sourceLocation.id] = {
                source: destinationLocation,
                destination: sourceLocation,
                distance: +flight.distance / 10
            };
    }

    // throw out any cities with too few links
    const pruned = prune(prune(prune(locations)));

    return { locations: pruned };
}

function prune(locations: PingLocation[]) {
    let wheat: PingLocation[] = [];
    let chaff: PingLocation[] = [];
    for (const loc of locations) {
        if (!loc) continue;
        if (loc.links.reduce((count, k) => count+ +!!k, 0) > 2) {
            wheat[loc.id] = loc;
        }
        else {
            chaff.push(loc);
        }
    }
    // unlink the chaff from the wheat
    for (const loc of chaff) {
        const links = loc.links
        for (const link of links) {
            if (!link) continue;
            link.destination.links[loc.id] = null;
            link.source.links[loc.id] = null;
        }
    }

    console.log(`${chaff.length} isolated locations removed`)
    return wheat;
}