import moment = require("moment");

export function Load() {
    const rawData: QuantasFlightInfo[] = require('../Quantas.json');
    const timezones: FlightTimezone[] = require('../timezones.json');
    let skipped = 0;

    // adjust all the times for timezone differences
    const adjustedFlights: FlightSummary[] = [];
    for (const flight of rawData) {
        const fromTimezone = timezones.find(t => t.name === flight.from);
        const toTimezone = timezones.find(t => t.name === flight.to);
        if (fromTimezone === undefined || toTimezone === undefined) {
            skipped++;
            continue;
        }
        const departTime = moment({
            day: 1 + Math.floor(flight.depart / 2400),
            hours: Math.floor(flight.depart / 100) % 24,
            minutes: flight.depart % 100
        }).subtract(fromTimezone.timezone, 'hours');
        const arriveTime = moment({
            day: 1 + Math.floor(flight.arrive / 2400),
            hours: Math.floor(flight.arrive / 100) % 24,
            minutes: flight.arrive % 100
        }).subtract(toTimezone.timezone, 'hours');

        const flightTime = moment.duration(arriveTime.diff(departTime)).asMinutes();
        if (flightTime <= 0) continue;
        adjustedFlights.push({
            source: flight.from,
            destination: flight.to,
            distance: flightTime
        });
        adjustedFlights.push({
            source: flight.to,
            destination: flight.from,
            distance: flightTime
        });
    }
    console.debug(`SKIPPED ${skipped} flights with unknown timezones`);

    // convert the raw data into something more usable
    let locations: PingLocation[] = [];

    for (const flight of adjustedFlights) {
        if (!locations.some(loc => loc.name === flight.source))
            locations.push({
                id: locations.length,
                name: flight.source,
                country: '',
                computed: false,
                links: [],
                x: 0,
                y: 0,
                z: 0
            });
    }

    for (const flight of adjustedFlights) {
        const sourceLocation = locations.find(loc => loc.name === flight.source);
        const destinationLocation = locations.find(loc => loc.name === flight.destination);
        if (!sourceLocation || !destinationLocation) continue;
        if (!sourceLocation.links[destinationLocation.id])
            sourceLocation.links[destinationLocation.id] = {
                source: sourceLocation,
                destination: destinationLocation,
                distance: flight.distance
            };
    }

    // throw out any cities with too few links
    const pruned = prune(prune(prune(locations)));

    return { locations: pruned };
}

function prune(locations: PingLocation[]) {
    let wheat: PingLocation[] = [];
    let chaff: PingLocation[] = [];
    for (const loc of locations) {
        if (!loc) continue;
        if (loc.links.reduce((count, k) => count+ +!!k, 0) > 1) {
            wheat[loc.id] = loc;
        }
        else {
            chaff.push(loc);
        }
    }
    // unlink the chaff from the wheat
    for (const loc of chaff) {
        const links = loc.links
        for (const link of links) {
            if (!link) continue;
            link.destination.links[loc.id] = null;
            link.source.links[loc.id] = null;
        }
    }

    console.log(`${chaff.length} isolated locations removed`)
    return wheat;
}