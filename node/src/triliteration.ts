// Based on https://en.wikipedia.org/wiki/True_range_multilateration

console.log(new Date())
import { writeFileSync } from "fs";

//import { Load } from "./generatePlane";
//import { Load } from "./generateSphere";
import { Load } from "./loadPings";
//import { Load } from "./loadQuantas";
//import { Load } from "./loadFlights";

// load the raw data in a usable format
const { locations } = Load();

/** /
// choose the initial points at random
const randPt = (used: number[]) => {
    let pt: number;
    while (true) {
        pt = (locations.length * Math.random()) | 0;
        if (!locations[pt] || used.indexOf(pt) >= 0) continue;
        let missingLink = false;
        for (const o of used)
            if (!locations[o].links[pt]) {
                missingLink = true;
                break;
            }
        if (missingLink) continue;
        return locations[pt];
    }
};
const a = randPt([]);
const b = randPt([a.id]);
const c = randPt([a.id,b.id]);
const d = randPt([a.id,b.id,c.id]);

/*/
// take sensible, widespread initial points
const a = locations[100]; // San Francisco
const b = locations[  6]; // Tokyo
const c = locations[ 42]; // Cape Town
const d = locations[ 21]; // Brisbane
/** /

// choose the initial points based on how well connected they are
const randPt = (used: number[]) => {
    let pt: number;
    while (true) {
        pt = (locations.length * Math.random()) | 0;
        if (!locations[pt] || used.indexOf(pt) >= 0) continue;
        let missingLink = false;
        for (const o of used)
            if (!locations[o].links[pt]) {
                missingLink = true;
                break;
            }
        if (missingLink) continue;
        return locations[pt];
    }
};
const a = randPt([]);
const b = randPt([a.id]);
const c = randPt([a.id,b.id]);
const d = randPt([a.id,b.id,c.id]);

/**/


a.computed = true;
b.computed = true;
c.computed = true;
b.x = a.links[b.id].distance;
const r1 = a.links[c.id].distance;
const r2 = b.links[c.id].distance;
c.x = (r1*r1 - r2*r2 + b.x*b.x) / (2 * b.x);
c.y = Math.sqrt(r1*r1 - c.x*c.x);

console.log(`A: ${a.name}, ${a.country}:  ${a.links[b.id].distance} .. ${a.links[c.id].distance}`);
console.log(`B: ${b.name}, ${b.country}:  ${b.links[a.id].distance} .. ${b.links[c.id].distance}`);
console.log(`C: ${c.name}, ${c.country}:  ${c.links[a.id].distance} .. ${c.links[b.id].distance}`);


// naively construct the rest of the cities, assuming the distances are all perfect
computeSlants(a, b, c, null, d);
console.log(`D: ${d.name}, ${d.country}:  ${d.links[a.id].distance} .. ${d.links[b.id].distance} .. ${d.links[c.id].distance}`);
for (const loc of locations) {
    if (!loc || loc.computed) continue;
    if (!loc.links[a.id] || !loc.links[b.id] || !loc.links[c.id]) {
        console.log(`Missing links to city: ${loc.name}, ${loc.country}`);
        continue;
    }
    computeSlants(a, b, c, d, loc);
}

// dump the results to console
console.log('\n\n');
for (const k of locations)
    if (k && k.computed) {
        console.log(`${k.x.toFixed(2)}, ${k.y.toFixed(2)}, ${k.z.toFixed(2)}, ${k.name}, ${k.country}`)
    }
console.log(new Date())
const output =JSON.stringify(locations.filter(k => k && k.computed).map(k => ({
    x: +k.x.toFixed(4), y: +k.y.toFixed(4), z: +k.z.toFixed(4), name: k.name + ' ' + k.country
})));
writeFileSync('triler.json', output);

//#region Details
/**
 * 
 * @param a The location at  0,  0, 0
 * @param b The location at x1,  0, 0
 * @param c The location at x2, y2, 0
 * @param d Any existing location used to choose between the two possible computed locations
 * @param loc The location to be computed from the distances to the others
 */
function computeSlants(a: PingLocation, b: PingLocation, c: PingLocation, d: PingLocation, loc: PingLocation) {
    if (!isTriangle(loc.links[a.id].distance, loc.links[b.id].distance, a.links[b.id].distance)
        || !isTriangle(loc.links[a.id].distance, loc.links[c.id].distance, a.links[c.id].distance)
        || !isTriangle(loc.links[b.id].distance, loc.links[c.id].distance, b.links[c.id].distance)) {
        console.log(`Nonsense location could not be computed for ${loc.name}, ${loc.country}`);
        return;
    }
    // TODO: allow for `a` and `b` to _not_ be the original two points
    const v = c.x * c.x + c.y * c.y;
    const r1 = loc.links[a.id].distance ** 2;
    const r2 = loc.links[b.id].distance ** 2;
    const r3 = loc.links[c.id].distance ** 2;
    loc.x = (r1 - r2 + b.x*b.x) / (2 * b.x);
    loc.y = (r1 - r3 + v - 2 * c.x * loc.x) / (2 * c.y);
    loc.z = Math.sqrt(r1 - loc.x*loc.x - loc.y*loc.y);
    loc.computed = !!loc.z;

    // figure out if the negative sqrt is the correct solution based on some fourth "known" point
    if (loc.computed && d && d.computed && loc.links[d.id]) {
        const partialDistance = (loc.x-d.x) ** 2 + (loc.y-d.y) ** 2 - loc.links[d.id].distance ** 2;
        if (Math.abs(partialDistance + (loc.z+d.z)**2) < Math.abs(partialDistance + (loc.z-d.z) ** 2))
            loc.z = -loc.z;
    }
}

/**
 * Checks the side lengths to see if they could be formed into a triangle
 */
function isTriangle(side1: number, side2: number, side3: number) {
    if (side1 > side2) {
        if (side1 > side3)
            return side2 + side3 > side1;
    }
    else if (side2 > side3)
        return side1 + side3 > side2;
    return side1 + side2 > side3;
}
//#endregion