interface PingData {
    pingData: {
        [src: string]: PingDataSource
    }
}
interface PingDataSource {
    [dest: string]: PingInfo
}
interface PingInfo {
    id: string;
    avg: string;
    max: string;
    min: string;
    country: string;
    source_name: string;
    destination_name: string;
    destination_id: string;
}

/** { from:"KJFK", to:"KLAX", depart:"12:00", arrive:"18:10", distance:"2148" } */
interface FlightInfo {
    from: string;
    to: string;
    depart: string;
    arrive: string;
    distance: string;
}

interface QuantasFlightInfo {
    from: string;
    to: string;
    depart: number;
    arrive: number;
}
interface FlightTimezone {
    name: string;
    timezone: number;
}
interface FlightSummary {
    distance: number;
    source: string;
    destination: string;
}


interface Vector {
    x: number;
    y: number;
    z: number;
}
interface PingLocation extends Vector {
    id: number;
    country: string;
    name: string;
    links: PingLink[];
    computed: boolean;
}
interface PingLink {
    distance: number;
    source: PingLocation;
    destination: PingLocation;
}

interface ForcedLocation extends PingLocation {
    forces: Vector & { count: number };
    absForce?: Vector;
}