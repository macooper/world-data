import { writeFileSync } from "fs";

//import { Load } from "./generatePlane";
//import { Load } from "./generateSphere";
//import { Load } from "./loadPings";
//import { Load } from "./loadQuantas";
import { Load } from "./loadFlights";

const stepSize = 10;
const stepCount = 100000;


// load the raw data in a usable format
const { locations } = Load();
const zero = () => ({x:0, y:0, z:0, count:0});
const points = locations.filter(p => !!p && p.name !== "Kampala")
    .map<ForcedLocation>(p => ({ ...p, forces: zero() }));

// initialize the positions of the points arbitrarily
for (const k of points) {
    const name = k.name + "ZZZ";
    k.x = name.charCodeAt(0);
    k.y = name.charCodeAt(1);
    k.z = name.charCodeAt(2);
    k.absForce = zero();
}


// simulate forces driving the points to the correct positions
printAll();
for (let step=0; step < stepCount; step++) {
    const totalShift = simulateStep(stepSize)
    if (isNaN(totalShift) || totalShift > 1000000) {
        console.error('Solution diverging!');
        break;
    }
}

// dump the results to console
printAll();
// and save them to a file
const output = JSON.stringify(points.map(k => ({
    x: +k.x.toFixed(4), y: +k.y.toFixed(4), z: +k.z.toFixed(4), name: (k.name + ' ' + k.country).trim()
})));
writeFileSync('bal.json', output);

//#region Details
function simulateStep(stepSize: number) {
    // compute the "forces"
    points.forEach(p => {
        p.absForce = zero();
        p.forces = points.reduce(
            (f, o) => {
                if (p === o || !p.links[o.id] || !p.links[o.id].distance) return f;
                const diff = subtract(p, o);
                const distance = magnitude(diff);
                if (distance === 0) return f;
                const offset = p.links[o.id].distance - distance;

                // force as a linear function of the distance
                //const force = multiply(diff, offset / distance);

                // force decreases with distance
                const force = multiply(diff, offset / (distance * p.links[o.id].distance));

                p.absForce = add(p.absForce, abs(force));
                return {
                    ...add(f, force),
                    count: f.count+1
                }
            },
            zero()
        )
    });

    // use the forces to move the points
    let totalMovement = 0;
    let totalForce = 0;
    points.forEach(p => {
        if (p.forces.count === 0) return;
        const shift = multiply(p.forces, stepSize / p.forces.count);
        totalMovement += magnitude(shift);
        totalForce += magnitude(p.absForce);
        p.x += shift.x;
        p.y += shift.y;
        p.z += shift.z;
    });

    console.log(`Shifted by ${totalMovement}\t\t\taverage force: ${totalForce/points.length}`);
    return totalMovement;
}

function add(a: Vector, b: Vector|number): Vector {
    if (typeof b === "number")
        return {
            x: a.x + b,
            y: a.y + b,
            z: a.z + b
        };
    return {
        x: a.x + b.x,
        y: a.y + b.y,
        z: a.z + b.z
    };
}
function subtract(a: Vector, b: Vector|number): Vector {
    if (typeof b === "number")
        return {
            x: a.x - b,
            y: a.y - b,
            z: a.z - b
        };
    return {
        x: a.x - b.x,
        y: a.y - b.y,
        z: a.z - b.z
    };
}
function magnitude(v: Vector) {
    return Math.sqrt(v.x**2 + v.y**2 + v.z**2);
}
function multiply(v: Vector, b: number): Vector {
    return {
        x: v.x * b,
        y: v.y * b,
        z: v.z * b
    };
}
function abs(a: Vector): Vector {
    return {
        x: Math.abs(a.x),
        y: Math.abs(a.y),
        z: Math.abs(a.z)
    };
}

function printAll() {
    console.log('\n\n');
    points.forEach(k =>
        console.log(`${k.x.toFixed(2)}, ${k.y.toFixed(2)}, ${k.z.toFixed(2)}, ${k.name}, ${k.country}`)
    );
}
//#endregion