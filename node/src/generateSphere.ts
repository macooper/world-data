export function Load() {
    const points = 250;
    const diameter = 200;

    const distanceErrorPercent = 0.25;
    const linkFillPercent = 0.7;

    let locations: PingLocation[] = [];

    for (let id=0; id<points; id++) {
        const pt: PingLocation = {
            id,
            computed: true,
            links: [],
            name: ""+id,
            country: "",
            x: Math.random()-.5,
            y: Math.random()-.5,
            z: Math.random()-.5
        };
        const scale = diameter / Math.sqrt(pt.x**2 + pt.y**2 + pt.z**2);
        pt.x = pt.x * scale;
        pt.y = pt.y * scale;
        pt.z = pt.z * scale;
        locations.push(pt);
    }
    for (const pt1 of locations) {
        for (let id2=pt1.id+1; id2<locations.length; id2++) {
            if (Math.random() > linkFillPercent) continue;
            const pt2 = locations[id2];
            const distance = Math.sqrt((pt1.x-pt2.x)**2 + (pt1.y-pt2.y)**2 + (pt1.z-pt2.z)**2) 
                * (2 * distanceErrorPercent * Math.random() + 1 - distanceErrorPercent);
            pt1.links[id2] = {
                source: pt1,
                destination: pt2,
                distance
            };
            pt2.links[pt1.id] = {
                source: pt2,
                destination: pt1,
                distance
            };
        }

        //console.log(`${pt1.x.toFixed(2)}\t${pt1.y.toFixed(2)}\t${pt1.z.toFixed(2)}`)

        pt1.x = pt1.y = pt1.z = 0;
        pt1.computed = false;
    }
    

    return { locations }
}